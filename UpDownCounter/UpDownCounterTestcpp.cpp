#include"UpDownCounter.h"
#include<iostream>
#include<Windows.h>
using namespace std;

int main()
{
	UpDownCounter data;
	int start, stop, choice,step;                                      //variables
	cout << "Enter choice" << endl;                                    //1. incement 2. decrement
	cin >> choice;
	switch (choice)
	{
	case 1: cout << "increment\n" << endl;
		cout << "Enter start time: " << endl;                          //starting time
		cin >> start;
		cout << "\nEnter the stop time: " << endl;                     //stopping time
		cin >> stop;
		data.increment(start, stop);                              //incrementing function
		break;
	case 2: cout << "decrement\n" << endl;                                  
		cout << "Enter start time: " << endl;                          //starting time
		cin >> start;
		cout << "\nEnter the stop time: " << endl;                     //stopping time
		cin >> stop;
		data.decrement(start, stop);                             //decrementing function
		break;
	default: cout << "Enter a valid choice:\n" << endl;            //error! when user enters an invalid choice
		break;
	}
	system("pause");
	return 0;
}